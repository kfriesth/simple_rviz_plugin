 [![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr) Simple RViz plugin
===

[![build status](https://gitlab.com/InstitutMaupertuis/simple_rviz_plugin/badges/kinetic-devel/build.svg)](https://gitlab.com/InstitutMaupertuis/simple_rviz_plugin/commits/kinetic-devel)

This is a ROS package of a simple RViz plugin enabling to move a Fanuc M10iA robot along it's X, Y or Z axis.

<img src="simple_rviz_plugin.png" align="center" height="300">

Directories in the project
---

| Directory  | Description
------------ | -----------
`simple_node` | Contains the definition and implementation of a `SendOffset` service.
`simple_rviz_plugin` | Defines a Qt widget with buttons that are connected to Qt slots, uses `SendOffset` to send a service request.

Dependencies
---
- [Robot Operating System](http://wiki.ros.org/ROS/Installation)
- [`industrial-core`](http://wiki.ros.org/industrial_core)
- [`fanuc`](http://wiki.ros.org/fanuc)

This package has been tested with Ubuntu 16.04 and ROS Kinetic.

Install
---
Install the dependencies by following the wiki instructions and cloning the repositories into your catkin workspace.

`cd` to your catkin workspace source directory:
```
git clone https://gitlab.com/InstitutMaupertuis/simple_rviz_plugin.git
cd ..
catkin build
```

Launching
---
Source `devel/setup.bash` and launch:

```
roslaunch simple_node simple_node_rviz.launch
```

What happens
---
The button `Move robot` is connected to the [moveRobotButtonHandler](/simple_rviz_plugin/src/simple_panel_widget.cpp#L99-122) slot, which fills a service request and launches a thread to send that request and wait for an answer.
Note that the panel will remain freezed until the response has been received.

For more details about why this is done like that, please read [Rviz panel and message passing](https://groups.google.com/d/msg/swri-ros-pkg-dev/nKf0h1O0h1o/AeJ8_va5DwAJ).

