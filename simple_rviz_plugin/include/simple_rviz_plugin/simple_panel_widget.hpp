#ifndef SIMPLE_PANEL_WIDGET_HPP
#define SIMPLE_PANEL_WIDGET_HPP

#ifndef Q_MOC_RUN
#include <ros/ros.h>
#include <ros/service.h>
#include <rviz/panel.h>
#endif

#include <simple_node/SendOffset.h> // Description of the service we will use

#include <simple_rviz_plugin/x_widget.hpp> // Description of each tab in simple panel
#include <simple_rviz_plugin/y_widget.hpp>
#include <simple_rviz_plugin/z_widget.hpp>

class QTabWidget;
class QLabel;
class QVBoxLayout;
class QPushButton;

namespace simple_rviz_plugin
{
class SimplePanelWidget : public rviz::Panel
{
Q_OBJECT
  public:
  SimplePanelWidget(QWidget* parent = 0);
  virtual ~SimplePanelWidget();

Q_SIGNALS:
  void displayStatus(const QString);
  void displayErrorMessageBox(const QString,
                              const QString,
                              const QString);

  public Q_SLOTS:
  virtual void moveRobot();

protected Q_SLOTS:
  virtual void load(const rviz::Config& config);
  virtual void save(rviz::Config config) const;
  virtual void triggerSave();

  void displayStatusHandler(const QString);
  void displayErrorBoxHandler(const QString title,
                              const QString message,
                              const QString info_msg);


  void moveRobotButtonHandler();
  void connectToServices();

protected:
  // Description of ROS NodeHandle, service client and service
  std::shared_ptr<ros::NodeHandle> nh_;
  ros::ServiceClient offset_move_robot_;
  simple_node::SendOffset srv_;

  QTabWidget* tab_widget_;
  std::vector<QWidget*> tab_widgets_;
  QLabel* status_label_;
  QLabel* status_;
  QPushButton* move_robot_button_;
};

}  // end namespace

#endif // SIMPLE_PANEL_WIDGET_H
