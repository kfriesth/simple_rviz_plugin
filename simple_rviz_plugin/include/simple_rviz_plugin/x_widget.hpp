#ifndef X_WIDGET_HPP
#define X_WIDGET_HPP

#ifndef Q_MOC_RUN
#include <ros/ros.h>
#include <ros/service.h>
#include <rviz/panel.h>
#endif

class QDoubleSpinBox;
class QLabel;
class QWidget;
class QHBoxLayout;

#include <simple_node/SendOffset.h>

namespace simple_rviz_plugin
{
class XWidget : public QWidget
{
Q_OBJECT
  public:
  XWidget(std::shared_ptr<ros::NodeHandle> nh,
          QWidget* parent = NULL);

  simple_node::SendOffset::Request getParams();
  void setParams(simple_node::SendOffset::Request params);

  void load(const rviz::Config& config);
  void save(rviz::Config config);

Q_SIGNALS:
  void displayStatus(const QString);
  void displayErrorMessageBox(const QString,
                              const QString,
                              const QString);

protected Q_SLOTS:
  virtual void triggerSave();
  void updateGUI();
  void updateInternalValues();

protected:
  std::shared_ptr<ros::NodeHandle> nh_;
  simple_node::SendOffset::Request params_;

  QDoubleSpinBox* x_offset_;
  QLabel* x_offset_label_;
  QHBoxLayout* x_offset_layout_;
};

} // End namespace

#endif // X_WIDGET_H
