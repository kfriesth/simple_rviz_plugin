#include <QTabWidget>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QDoubleSpinBox>
#include <QMessageBox>
#include <QFuture>
#include <QtConcurrent/QtConcurrentRun>

#include <simple_rviz_plugin/simple_panel_widget.hpp>

#include <simple_node/SendOffset.h> //Description of the service we will use

namespace simple_rviz_plugin
{
SimplePanelWidget::SimplePanelWidget(QWidget* parent) :
        rviz::Panel(parent),
        nh_(new ros::NodeHandle)
{
  // Create UI and layout element
  tab_widget_ = new QTabWidget();
  tab_widgets_.push_back(new XWidget(nh_));
  tab_widgets_.push_back(new YWidget(nh_));
  tab_widgets_.push_back(new ZWidget(nh_));

  for (auto tab_widget : tab_widgets_)
  {
    tab_widget_->addTab(tab_widget, tab_widget->objectName());
    connect(tab_widget, SIGNAL(displayStatus(const QString)), this, SLOT(displayStatusHandler(const QString)));
    connect(tab_widget, SIGNAL(displayErrorMessageBox(const QString, const QString, const QString)), this,
            SLOT(displayErrorBoxHandler(const QString, const QString, const QString)));
  }

  QLabel* status_label_ = new QLabel;
  status_label_->setText("Status:");
  status_ = new QLabel;
  QPushButton* move_robot_button_ = new QPushButton;
  move_robot_button_->setText("Move robot");

  QVBoxLayout* widget_layout = new QVBoxLayout();
  widget_layout->addWidget(tab_widget_);
  widget_layout->addWidget(move_robot_button_);
  widget_layout->addSpacing(10);
  widget_layout->addWidget(status_label_);
  widget_layout->addWidget(status_);
  setLayout(widget_layout);

  // Connect handlers
  connect(move_robot_button_, SIGNAL(released()), this, SLOT(moveRobotButtonHandler()));
  connect(this, SIGNAL(displayStatus(QString)), this, SLOT(displayStatusHandler(QString)));
  connect(this, SIGNAL(displayErrorMessageBox(const QString, const QString, const QString)), this,
          SLOT(displayErrorBoxHandler(const QString, const QString, const QString)));

  // Setup client
  offset_move_robot_ = nh_->serviceClient<simple_node::SendOffset>("move_robot_offset");

  // Check connection of client
  QFuture<void> future = QtConcurrent::run(this, &SimplePanelWidget::connectToServices);
}

SimplePanelWidget::~SimplePanelWidget()
{
}

void SimplePanelWidget::connectToServices()
{
  Q_EMIT setEnabled(false);

  // Check offset_move_robot_ connection
  Q_EMIT displayStatus("Connecting to service");
  while (ros::ok())
  {
    if (offset_move_robot_.waitForExistence(ros::Duration(2)))
    {
      ROS_INFO_STREAM("RViz panel connected to the service " << offset_move_robot_.getService());
      Q_EMIT displayStatus(
          QString::fromStdString("RViz panel connected to the service: " + offset_move_robot_.getService()));
      break;
    }
    else
    {
      ROS_ERROR_STREAM("RViz panel could not connect to ROS service:\n\t" << offset_move_robot_.getService());
      Q_EMIT displayStatus(
          QString::fromStdString("RViz panel could not connect to ROS service: " + offset_move_robot_.getService()));
      sleep(1);
    }
  }

  ROS_WARN_STREAM("Service connection have been made");
  Q_EMIT displayStatus("Ready to take commands");
  Q_EMIT setEnabled(true);
}

void SimplePanelWidget::displayStatusHandler(const QString status)
{
  status_->setText(status);
}

void SimplePanelWidget::displayErrorBoxHandler(const QString title,
                                               const QString message,
                                               const QString info_msg)
{
  Q_EMIT setEnabled(false);
  QMessageBox msg_box;
  msg_box.setWindowTitle(title);
  msg_box.setText(message);
  msg_box.setInformativeText(info_msg);
  msg_box.setIcon(QMessageBox::Critical);
  msg_box.setStandardButtons(QMessageBox::Ok);
  msg_box.exec();
  Q_EMIT setEnabled(true);
}

void SimplePanelWidget::moveRobotButtonHandler()
{
  // Fill the service request
  srv_.request = ((XWidget*)tab_widgets_[tab_widget_->currentIndex()])->getParams();

  // Run moveRobot function in a thread
  QFuture<void> future = QtConcurrent::run(this, &SimplePanelWidget::moveRobot);
}

void SimplePanelWidget::moveRobot()
{
  //WARNING: DO NOT modify UI in the thread!
  Q_EMIT setEnabled(false);

  // Call service
  bool success(offset_move_robot_.call(srv_));
  Q_EMIT displayStatus(QString::fromStdString(srv_.response.ReturnMessage));

  if (!success)
  {
    Q_EMIT displayErrorMessageBox("Cannot move robot", "Could not find a solution!", "Maybe the offset is too big?");
    Q_EMIT displayStatus("Failed to move robot");
    return;
  }

  Q_EMIT setEnabled(true);
}

void SimplePanelWidget::triggerSave()
{
  Q_EMIT configChanged();
}

void SimplePanelWidget::save(rviz::Config config) const
                             {
  rviz::Panel::save(config);
  //Each tab call his own save function
}

// Load all configuration data for this panel from the given Config object.
void SimplePanelWidget::load(const rviz::Config& config)
{
  rviz::Panel::load(config);
  // Each tab call his own load function
}

}  // end namespace

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(simple_rviz_plugin::SimplePanelWidget, rviz::Panel)
