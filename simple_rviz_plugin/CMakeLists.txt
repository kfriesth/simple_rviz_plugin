cmake_minimum_required(VERSION 2.8.3)
project(simple_rviz_plugin)
add_definitions(-std=c++11 -Wall -Wextra)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rqt_gui
  simple_node
  rviz
)

find_package(Qt5Widgets REQUIRED)
find_package(Qt5Core REQUIRED)
find_package(Qt5Gui REQUIRED)

catkin_package(
  INCLUDE_DIRS
  ${simple_rviz_plugin_INCLUDE_DIRECTORIES}
  LIBRARIES
  ${PROJECT_NAME}
  CATKIN_DEPENDS
  simple_node
  roscpp
  rqt_gui
  rviz
)

###########
## Build ##
###########

set(simple_rviz_plugin_SRCS
  src/simple_panel_widget.cpp
  src/x_widget.cpp
  src/y_widget.cpp
  src/z_widget.cpp 
)

set(simple_rviz_plugin_HDRS
  include/${PROJECT_NAME}/simple_panel_widget.hpp
  include/${PROJECT_NAME}/x_widget.hpp
  include/${PROJECT_NAME}/y_widget.hpp
  include/${PROJECT_NAME}/z_widget.hpp
)

#set(simple_rviz_plugin_UIS
#  src/simple.ui
#)

set(simple_rviz_plugin_INCLUDE_DIRECTORIES
  include
  ${catkin_INCLUDE_DIRS}
)

qt5_wrap_cpp(simple_rviz_plugin_MOCS ${simple_rviz_plugin_HDRS})
#qt5_wrap_ui(simple_rviz_plugin_UIS_H ${simple_rviz_plugin_UIS})

include_directories(
  ${simple_rviz_plugin_INCLUDE_DIRECTORIES}
  ${catkin_INCLUDE_DIRS}
)

## Declare a cpp library
add_library(${PROJECT_NAME}
#  ${simple_rviz_plugin_UIS_H}
  ${simple_rviz_plugin_MOCS}
  ${simple_rviz_plugin_SRCS}
)
target_link_libraries(
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
  Qt5::Widgets
  Qt5::Gui
)
add_dependencies(
  ${PROJECT_NAME}
  simple_node_generate_messages_cpp
)

find_package(class_loader)
class_loader_hide_library_symbols(${PROJECT_NAME})

#############
## Install ##
#############
install(TARGETS ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION})

install(DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
)
